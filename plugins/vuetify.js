// Vuetify.js
import 'vuetify/styles'
import { createVuetify } from 'vuetify'
import { aliases, mdi } from 'vuetify/iconsets/mdi-svg'
import {
  mdiAccount,
  mdiAlertCircle,
  mdiBrush,
  mdiCardAccountDetails,
  mdiChevronLeft,
  mdiCrown,
  mdiEmail,
  mdiFamilyTree,
  mdiFormatListChecks,
  mdiHandHeart,
  mdiHeartOutline,
  mdiLink,
  mdiNotebook,
  mdiOpenInNew,
  mdiPentagram,
  mdiPlaylistMusic,
  mdiSeal,
} from '@mdi/js'
import { brandSVGs, customSVGs } from '~/assets/allSvgs'

export default defineNuxtPlugin((nuxtApp) => {
  const fbnDarkTheme = {
    dark: true,
    colors: {
      background: '#121212',
      surface: '#1E1E1E',
      primary: '#3BA57E',
      secondary: '#424242',
      accent: '#AC6FBE',
      info: '#00AFDB',
      success: '#48DB6A',
      warning: '#FFCE30',
      error: '#FF5F47',
    },
  }

  const vuetify = createVuetify({
    ssr: true,
    // https://vuetifyjs.com/en/features/theme/
    theme: {
      defaultTheme: 'fbnDarkTheme',
      variations: {
        colors: ['primary', 'secondary', 'accent'],
        lighten: 1,
        darken: 1,
      },
      themes: { fbnDarkTheme },
    },
    // https://vuetifyjs.com/en/features/icon-fonts/
    icons: {
      // https://pictogrammers.com/library/mdi/
      defaultSet: 'mdi',
      aliases: {
        ...aliases,
        mdiAccount,
        mdiAlertCircle,
        mdiBrush,
        mdiCardAccountDetails,
        mdiChevronLeft,
        mdiCrown,
        mdiEmail,
        mdiFamilyTree,
        mdiFormatListChecks,
        mdiHandHeart,
        mdiHeartOutline,
        mdiLink,
        mdiNotebook,
        mdiOpenInNew,
        mdiPentagram,
        mdiPlaylistMusic,
        mdiSeal,
      },
      sets: {
        mdi,
        brand: brandSVGs,
        custom: customSVGs,
      },
    },
  })

  nuxtApp.vueApp.use(vuetify)
})
