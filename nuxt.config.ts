import vuetify from 'vite-plugin-vuetify'

// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: false },
  ssr: false,
  css: ['@/assets/css/main.css'],
  modules: [
    // eslint-disable-next-line require-await
    async (_options, nuxt) => {
      nuxt.hooks.hook('vite:extendConfig', (config) => {
        config?.plugins?.push(vuetify())
      })
    },
  ],
  build: {
    transpile: ['vuetify'],
  },
  vite: {
    define: {
      'process.env.DEBUG': false,
    },
  },
  // https://nuxt.com/docs/guide/going-further/runtime-config
  runtimeConfig: {
    public: {
      projectUrl:
        'https://gitlab.com/fadebynight-fans/fadebynight-fans.gitlab.io',
      supportMail:
        'contact-project+fadebynight-fans-fadebynight-fans-gitlab-io-support@incoming.gitlab.com',
    },
  },
  app: {
    head: {
      charset: 'utf-8',
      viewport: 'width=device-width, initial-scale=1',
      htmlAttrs: {
        lang: 'en',
      },
      link: [
        { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
        {
          rel: 'canonical',
          href: 'https://fadebynight.fans/',
        },
      ],
      meta: [
        // Google verification
        {
          name: 'google-site-verification',
          content: 'HgtaxMpXY0Zjba_W6DCqlgqpjdrp0f16kBHzn_G7FqU',
        },
        // Technical
        { name: 'format-detection', content: 'telephone=no' },
        { name: 'robots', content: 'index, follow' },
        // Generic
        {
          hid: 'description',
          name: 'description',
          content: 'A home for FadeByNightVA audio nuts and lore hoarders!',
        },
        {
          name: 'image',
          content: 'https://fadebynight.fans/preview.png',
        },
        {
          name: 'keywords',
          content:
            'FadeByNightVA, FadeByNight, fans, fansite, fan, site, community, guide, audio',
        },
        // Open Graph
        { property: 'og:type', content: 'website' },
        { property: 'og:title', content: 'FadeByNight Fans' },
        {
          property: 'og:description',
          content: 'A home for FadeByNightVA audio nuts and lore hoarders!',
        },
        { property: 'og:url', content: 'https://fadebynight.fans/' },
        {
          property: 'og:image',
          content: 'https://fadebynight.fans/preview.png',
        },
        // Twitter
        { name: 'twitter:card', content: 'summary' },
        {
          name: 'twitter:image',
          content: 'https://fadebynight.fans/preview.png',
        },
      ],
    },
    pageTransition: { name: 'page', mode: 'out-in' },
    layoutTransition: { name: 'layout', mode: 'out-in' },
  },
})
