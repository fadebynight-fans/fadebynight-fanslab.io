# Developer README

[< Return to main README](../README.md)

**Index:**

- [Developer README](#developer-readme)
  - [Relevant Frameworks](#relevant-frameworks)
  - [Required and Recommended Tools](#required-and-recommended-tools)
    - [VS Code (Visual Studio Code)](#vs-code-visual-studio-code)
  - [First Time Setup](#first-time-setup)
  - [Development Workflow](#development-workflow)

---

So, you want to try contributing to the website's codebase?
Great, welcome aboard!

Whether you want to be an ongoing member of the team, or just want to submit one small fix, your efforts are welcome!

With the magic of GitLab and open source development, you don't even need to be a formal member of the project or group to contribute!
Just create a personal "fork" of the project, where you will have full read/write permissions.
Then, when you're ready, create a merge request on the main project, and you can submit changes from your personal copy!

> **Note:** This isn't intended to be a beginner's guide.
> You should be familiar with using GitLab and `git` before you begin.
> Some familiarity with `npm` will also help you feel more comfortable with the setup, but the necessary commands will be provided below.

## Relevant Frameworks

The site is built on the following Javascript frameworks.
Depending on how deep you want to dive into the codebase, you may want to be famliar with them before you get started.

- [Vue.js](https://vuejs.org/guide/introduction.html)
  (The core of the entire project)
- [Nuxt](https://nuxt.com/docs/getting-started/introduction)
  (Provides Vue project structure and abstraction)
- [Vuetify](https://vuetifyjs.com/en/introduction/why-vuetify/)
  (Provides Vue UI components)

## Required and Recommended Tools

To efficiently contribute to the project, you will need to have the following installed on your system before you get started:

- [Git](https://git-scm.com/)
  (for version control)
- [Node.js](https://nodejs.org/en)
  (to provide supporting packages via `npm`)

You will also need your editor/IDE of choice.

> We _highly_ recommend using [VS Code](https://code.visualstudio.com/)! (See below for more information.)

### VS Code (Visual Studio Code)

If you decide to use VS Code for this project, there are a few important things to know:

Firstly, the project has certain VS Code specific settings and extension recommendations included.
(This should make it much easier to get started!)
When you open the project workspace for the first time, VS Code will prompt you to install the recommended extensions.
You can also review the list at any time with the `Extensions: Show Recommended Extensions` command from the Command Palette.

Secondly, in order to use the recommended Volar extension's full functionality, there is one configuration step you will have to do manually.
Please refer to the Vue documentation regarding [Volar Takeover Mode](https://vuejs.org/guide/typescript/overview.html#volar-takeover-mode), and follow the instructions.

## First Time Setup

Use the appropriate `git` command to clone the project to your local filesystem.

Then, from within the project directory, install the necessary packages with the following:

```sh
# First time setup
npm install
```

## Development Workflow

You can run a local development server that updates in real-time while you edit the codebase:

```sh
# Live development preview
npm run dev
```

For closer-to-production testing, you can create a local build:

```sh
# Test production build
npm run generate

# Preview build locally
npm run preview
```

Before submitting a merge request, _please_ use our linters to check your code for errors and formatting consistency:

```sh
# Code checking
npm run lint

# If the check fails, try our automated code cleanup
npm run lintfix
```
