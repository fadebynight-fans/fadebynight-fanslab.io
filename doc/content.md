# Content Contributor README

[< Return to main README](../README.md)

**Index:**

- [Content Contributor README](#content-contributor-readme)
  - [What kind of content can I submit?](#what-kind-of-content-can-i-submit)
    - [What if I don't want to host the content myself?](#what-if-i-dont-want-to-host-the-content-myself)
    - [What about NSFW?](#what-about-nsfw)
  - [How do I submit my content?](#how-do-i-submit-my-content)
  - [What about fan art?](#what-about-fan-art)

---

So, you have something you want to see featured on the site?
Great, we'd love to see it!

## What kind of content can I submit?

The idea is that material featured on the site should be "useful/informative" to fans of FadeByNightVA.
(You can use the current content as a point of reference.)

In general, this excludes most "fan art", be it visual, audio, or written word.
(But if your content falls into this category, we have an alternative!
[See our section about fan art!](#what-about-fan-art))

The content should also be hosted at a dedicated link (e.g. an online post or Google Doc).
The current fansite is a directory of links, not a place where the content is actually hosted.
This was done intentionally to allow you to maintain control of your content, leaving you free to update it as you please, without us standing in the way!

> **Safety/Privacy Notice!**
>
> Be conscious of where your content is hosted and how the platform works.
> Don't accidentally share any personal information you don't intend to!
>
> As a common example:  
> Google Docs are convenient, but did you know that the standard sharing link will expose the associated account/email to everyone who visits?!
> If you want to avoid this, you can use the "Publish to web" feature provided by Google Docs instead!
> (The document will be displayed differently, however, so make sure to preview the results.)

### What if I don't want to host the content myself?

[Contact us][email] and we'll see what we can do!

### What about NSFW?

We want the fan site itself to remain accessible to all FadeByNight Fans, regardless of the platform they listen to.

Most content that would be relevant to this site shouldn't be a problem.

For anything more questionable...
If the target site has appropriate age restrictions/warnings in place, that should be sufficient.
Otherwise, we would need to add other features to the site before we can allow that.
If that's something you'd really like to see supported, [contact us][email] and we'll see what we can do!

## How do I submit my content?

Just send us an [email][email] (or a [GitLab issue][issue] if you have an account)!  
Make sure to provide the following information:

1. The link to your content
2. A brief description of the content
3. Your name as you want it to appear in the credits
4. A link to your social media (Linktree works great!)

We'll get back to you with any questions/feedback, and to let you know when the link goes live!

## What about fan art?

We love fan art!
That's why we have a dedicated set of links to fan art tagged across multiple social media platforms.
If you want people to find your art through our links, just be sure to add `#FadeByNightFanArt` to your posts!

Don't see your platform of choice in our links?
Tag your post with `#FadeByNightFanArt` anyway, and [let us know][email]!
As long as there is content to feature, we're always happy to add more platforms to the list.

(We're considering adding an embedded fan art gallery in the future, but it will still be powered by that hashtag.
So keep tagging all of your posts! `#FadeByNightFanArt`!)

[email]: mailto:contact-project+fadebynight-fans-fadebynight-fans-gitlab-io-support@incoming.gitlab.com "Send an email"
[issue]: https://gitlab.com/fadebynight-fans/fadebynight-fans.gitlab.io/-/issues/new "Create a GitLab issue"
