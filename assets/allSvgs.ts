import { h } from 'vue'
import type { IconSet, IconProps } from 'vuetify'

import IBluesky from './brandSvgs/IBluesky.vue'
import IDeviantArt from './brandSvgs/IDeviantArt.vue'
import IGitLab from './brandSvgs/IGitLab.vue'
import IInstagram from './brandSvgs/IInstagram.vue'
import ILinktree from './brandSvgs/ILinktree.vue'
import ITwitter from './brandSvgs/ITwitter.vue'
import IXTwitter from './brandSvgs/IXTwitter.vue'
import IYouTube from './brandSvgs/IYouTube.vue'

import IFbnf from './customSvgs/IFbnf.vue'
import IFbnfHeart from './customSvgs/IFbnfHeart.vue'

// Brand icons
const brandSvgNameToComponent: any = {
  IBluesky,
  IDeviantArt,
  IGitLab,
  IInstagram,
  ILinktree,
  ITwitter,
  IXTwitter,
  IYouTube,
}
const brandSVGs: IconSet = {
  component: (props: IconProps) => h(brandSvgNameToComponent[props.icon]),
}
export { brandSVGs /* aliases */ }

// Custom icons
const customSvgNameToComponent: any = {
  IFbnf,
  IFbnfHeart,
}
const customSVGs: IconSet = {
  component: (props: IconProps) => h(customSvgNameToComponent[props.icon]),
}
export { customSVGs /* aliases */ }
