# FadeByNight Fans

Welcome to the inner workings of our fansite!
(<https://fadebynight.fans/>)

## Who are we?

The [fansite development team][devs] is... just me for now.
Hi, I'm Mist, FadeByNight aficionado since 2021!
I'm also a full-time software developer, so now I'm using those powers for the good of this lovely community.

The content featured on the site, however, comes from different contributors.
Each listing includes social links for the author(s) responsible for creating and maintaining the material independently of this site.
This project would mean little without their efforts, so please show them some love too!

## Want to contribute to the project?

Review the README that best fits you:

- [I'm a fan with content I want featured on the site.](doc/content.md)
- [I'm a developer who wants to contribute to the site's code base.](doc/developer.md)

## Other Questions / Comments / Concerns?

Please feel free to [email us!][email]

Alternatively, if you have a GitLab account, you can also [submit a new Issue][issue] directly to the project.

[devs]: https://gitlab.com/fadebynight-fans/fadebynight-fans.gitlab.io/-/project_members "Project members"
[email]: mailto:contact-project+fadebynight-fans-fadebynight-fans-gitlab-io-support@incoming.gitlab.com "Send an email"
[issue]: https://gitlab.com/fadebynight-fans/fadebynight-fans.gitlab.io/-/issues/new "Create a GitLab issue"
